const fs = require('fs')
const util = require('util')

const readFile = util.promisify(fs.readFile)

//? Promise based
readFile('promise-constructor.js', 'utf8')
  .then(file => {
    console.log(file)
  })
  .catch(error => {
    console.log(error)
  })
